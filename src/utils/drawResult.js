export default function drawResult(boxes) {
    // number of clicked boxes
    let count = 0;

    // iterate over alll boxes
    boxes.forEach(item => {
        if(item !== null) {
            count++;
        }
    });

    // check if all boxes are clicked
    if(count === 9) {
        return true
    } else {
        return false
    }
}