import React from 'react';

import { BrowserRouter, Route } from 'react-router-dom';

import Board from './components/BoardComponent';
import Input from './components/InputBoardComponent';

import './assets/bootstrap.css';

function App() {
  return (
    <div>
      <BrowserRouter>
        <Route exact path='/' component={Input}/>
        <Route path='/board' component={Board}/>
      </BrowserRouter>
    </div>
  );
}

export default App;
