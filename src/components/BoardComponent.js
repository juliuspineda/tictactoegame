import React, { useState } from 'react';
import { Link } from 'react-router-dom';

import Box from '../components/BoxComponent';
import Scoreboard from '../components/ScoreboardComponent';
import findWinner from '../utils/findWinner';
import drawResult from '../utils/drawResult';

import axios from 'axios';
import '../assets/style.css';

function BoardComponent() {
    let getLocalData = JSON.parse(localStorage.getItem('names'));

    const [profile] = useState(getLocalData); // profile name
    const [box,setBoxes] = useState(Array(9).fill(null)); // initial array of null
    const [history,setHistory] = useState([]); // store actions of players
    const [isNext, setNext] = useState(true); // next player
    const [scoreboard, setScoreboard] = useState(false);
    const [status, setStatus] = useState('');

    const handleOnClick = (index) => {
        // get the new data array
        const currentBoxes = box.slice()
        if(findWinner(currentBoxes) || currentBoxes[index]){
            return
        }

        if(drawResult(currentBoxes) === true) {
            return
        }

        currentBoxes[index] = isNext ? 'x' : 'o';

        history.push(isNext ? profile.player1 : profile.player2)

        setBoxes(currentBoxes)
        setHistory(history)
        setNext(!isNext)

        const winner = findWinner(currentBoxes)
        const allClicked = drawResult(currentBoxes)
 
        let status = '';
        if(winner) {
            status = `The winner is: ${(winner === 'x') ? profile.player1 : profile.player2 }!`;
            setStatus(status);
            setScoreboard(true);
            storedData((winner === 'x') ? profile.player1 : profile.player2, (winner !== 'x') ? profile.player1 : profile.player2, 'winner');
        } else if(!winner && allClicked) {
            status = `Game Drawn`
            setStatus(status);
            setScoreboard(true);
            storedData((winner === 'x') ? profile.player1 : profile.player2, (winner !== 'x') ? profile.player1 : profile.player2, 'draw');
        } else {
            status = `It is ${ (isNext) ? profile.player1 : profile.player2 }'s turn.`
            setStatus(status);
        }
    }

    const boardRestart = () => {
        setBoxes(Array(9).fill(null))
        setHistory([])
        setNext(true)
        setScoreboard(false)
    }

    const AlignText = (a,b) => {
        const ObjectReturn = {
            win: a.toLowerCase()
            .split(' ')
            .map((s) => s.charAt(0).toUpperCase() + s.substring(1))
            .join(' '),
            lose: b.toLowerCase()
            .split(' ')
            .map((s) => s.charAt(0).toUpperCase() + s.substring(1))
            .join(' '),
        }

        return ObjectReturn;
    }

    const storedData = (a,b,c) => {
        let WIN_SCORE = 100;
        let LOSE_SCORE = -100;
        let DRAW_SCORE = 0;

        let objName = AlignText(a,b)

        let temp = [];

        if(c === 'winner') {
            temp.push({
                name: objName.win,
                score: WIN_SCORE
            },{
                name: objName.lose,
                score: LOSE_SCORE
            });
        } else {
            temp.push({
                name: objName.win,
                score: DRAW_SCORE
            },{
                name: objName.lose,
                score: DRAW_SCORE
            });
        }

        let data = {
            players: temp
        }

        // post data in the api -> server -> database
        axios.post(`http://localhost:5000/api/result`, data)
        
    }

    return (
        <div className="main-container">
            <div className="d-flex" style={{ width: '500px' }}>
                <div className="col-lg-8 col-xl-8">
                    <div className="div-custom-form">
                        <div className="d-flex">
                            <Box 
                                onClick={ () => handleOnClick(0) }
                                value={ box[0] }
                            />
                            <Box 
                                onClick={ () => handleOnClick(1) }
                                value={ box[1] }
                            />
                            <Box 
                                onClick={ () => handleOnClick(2) }
                                value={ box[2] }
                            />
                        </div>
                        <div className="d-flex">
                            <Box 
                                onClick={ () => handleOnClick(3) }
                                value={ box[3] }
                            />
                            <Box 
                                onClick={ () => handleOnClick(4) }
                                value={ box[4] }
                            />
                            <Box 
                                onClick={ () => handleOnClick(5) }
                                value={ box[5] }
                            />
                        </div>
                        <div className="d-flex">
                            <Box 
                                onClick={ () => handleOnClick(6) }
                                value={ box[6] }
                            />
                            <Box 
                                onClick={ () => handleOnClick(7) }
                                value={ box[7] }
                            />
                            <Box
                                onClick={ () => handleOnClick(8) }
                                value={ box[8] }
                            />
                        </div>  
                        <div className="mt-3">
                            <h4>{status}</h4>
                            {
                                scoreboard && <div className="w-100">
                                    <button
                                        onClick={ boardRestart }
                                        className="btn btn-primary w-100"
                                    >Start new game</button>
                                </div>
                            }

                            {
                                scoreboard && <Scoreboard />
                            }

                            <Link to='/'>
                                Back to Input Name
                            </Link>
                        </div>
                    </div>
                </div>
                
                <div className="col-lg-4 col-xl-4">
                    <p style={{ fontSize: '16px', fontWeight: '900', fontFamily: 'Open Sans, sans-serif' }}>Player's History</p>
                    { history && history.length === 0 && <span>No moves to show.</span> }
                    { history && history.length !== 0 && history.map((data,key) => (
                        <div key={key}>
                            <p style={{ fontSize: '12px', fontWeight: '700', fontFamily: 'Open Sans, sans-serif' }}>{ (key + 1) + ') ' + data}'s move</p>
                        </div>
                    )) }
                </div>
                
            </div>
        </div>
    )
}

export default BoardComponent;
