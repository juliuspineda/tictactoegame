import React, {useState} from 'react'

import '../assets/bootstrap.css';

import { useHistory } from 'react-router-dom';

function InputBoardComponent(props) {

    let history = useHistory();

    const [profile, setProfile] = useState({}); // profile name

    const onChange = (e) => {
        e.persist();

        setProfile({...profile, [e.target.name]: e.target.value })
    }

    const onSubmit = (e, names = 'names') => {
        e.preventDefault();
        if(profile) {
            
            // I stored the data in local Storage
            localStorage.setItem(names, JSON.stringify(profile))
            
            // Redirect to another route
            history.push('/board')
        } else {

            // initial
            localStorage.setItem(names, {})
        }
    }

    let dt = new Date;
    let getDT = dt.getFullYear();

    return (
        <div className="main-container">
            <div className="div-custom-form">
                <div className="text-center mb-4">
                    <h3>Tic-Tac-Toe</h3>
                </div>
                <form onSubmit={onSubmit}>
                <div className="form-group">
                    <label className="small">Player 1</label>
                    <input 
                        type="text"
                        name="player1"
                        placeholder="input your name ..."
                        onChange={ onChange }
                        value={ profile.player1 || '' } 
                        className="form-control"
                        required
                    />
                </div>
                <div className="form-group">
                    <label className="small">Player 2</label>
                    <div>
                        <input 
                            type="text"
                            name="player2"
                            placeholder="input your name ..."
                            onChange={ onChange }
                            value={ profile.player2 || '' }
                            className="form-control"
                            required
                        />
                    </div>
                </div>
                <button 
                    type="submit"
                    className="btn btn-success w-100"
                >
                    Start
                </button>
                </form>
                <div className="form-group text-center mt-4">
                    <span className="small">Made By Julius Pineda { getDT }</span>
                </div>  
            </div>
        </div>
    )
}

export default InputBoardComponent;
