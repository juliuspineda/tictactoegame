import React from 'react'
import '../assets/style.css';

const BoxComponent = (props) => {
    return (
        <button 
            className="flex-1 box-size"
            style={{ fontSize: '35px', fontWeight: '900', color: '#FF5722' }}
            onClick={props.onClick}
        >
            {props.value}
        </button>
    )
}

export default BoxComponent;
