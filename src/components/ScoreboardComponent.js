import React, { useState,useEffect } from 'react'
import axios from 'axios'

function ScoreboardComponent() {
    const [score, setScore] = useState([]);
    useEffect(() => {
        const getData = async () => {
            const res = await axios(`http://localhost:5000/api/scoreboard`)
            
            let temp = [];
            for(let i = 0; i < res.data.length; i++){
                res && res.data[i] && res.data[i].players && res.data[i].players.map(data => (
                    temp.push(data)
                ))
            }

            temp.sort((a, b) => {return b.score - a.score});
            setScore(temp)   
        }

        getData()
    },[]);

    return (
        <div className="mt-3 mb-3 sboard">
            <p style={{ fontSize: '16px', fontWeight: '900', fontFamily: 'Open Sans, sans-serif' }}>Highscore</p>
            {
                score && score.map((data,key) => (
                    <div key={data._id} style={{ display: 'flex' }}>
                        <p>{ key + 1 + '.) ' + data.name}</p>
                <p style={{ marginLeft: '2rem' }}>{'-> ' + data.score + 'pts'} -- {(data.score === 100) ? 'winner' : (data.score === (-100)) ? 'lose' : 'drawn'}</p>
                    </div>
                ))
            }
        </div>
    )
}

export default ScoreboardComponent
